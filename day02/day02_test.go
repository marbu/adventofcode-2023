package main

import (
	"testing"
)

func TestParseGameLineNil(t *testing.T) {
	_, err := parseGameLine("")
	if err == nil {
		t.Fatal("error not reported")
	}
}

func TestParseGameLineNotLineYouLookFor(t *testing.T) {
	var tests = []string{
		"Something completelly different!",
		"too:many:colon:characters",
		"Game 1: not a game",
	}
	for _, input_line := range tests {
		t.Run(input_line, func(t *testing.T) {
			_, err := parseGameLine(input_line)
			if err == nil {
				t.Fatal("error not reported")
			}
		})
	}
}

func TestParseGameLineGameId(t *testing.T) {
	game, err := parseGameLine("Game 11: 2 red, 3 green")
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	want := 11
	if game.Id != want {
		t.Fatalf("wrong game.Id '%d' reported (want %d)", game.Id, want)
	}
}

func TestParseGameLineColorsSimple(t *testing.T) {
	game, err := parseGameLine("Game 11: 3 green, 2 red, 31 blue")
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	if len(game.Sets) != 1 {
		t.Fatalf("wrong number of game sets")
	}
	if game.Sets[0].R != 2 {
		t.Fatalf("wrong number of red cubes")
	}
	if game.Sets[0].G != 3 {
		t.Fatalf("wrong number of green cubes")
	}
	if game.Sets[0].B != 31 {
		t.Fatalf("wrong number of blue cubes")
	}
}

func TestGetMinimalCubeSetNil(t *testing.T) {
	var emptyGame Game
	var cs GameSet = emptyGame.getMinimalCubeSet()
	if cs.R != 0 {
		t.Fatalf("wrong number of red cubes")
	}
	if cs.G != 0 {
		t.Fatalf("wrong number of green cubes")
	}
	if cs.B != 0 {
		t.Fatalf("wrong number of blue cubes")
	}
}
