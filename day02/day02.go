package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Game struct {
	Id   int
	Sets []GameSet
}

type GameSet struct {
	R int
	G int
	B int
}

func parseGameLine(raw_line string) (Game, error) {
	var game Game
	var line_parts []string

	line_parts = strings.Split(raw_line, ":")
	if len(line_parts) != 2 {
		return game, errors.New("input string doesn't contain single ':' char")
	}
	fmt.Sscanf(line_parts[0], "Game %d", &game.Id) // TODO: error checking

	for _, set_str := range strings.Split(line_parts[1], ";") {
		var gs GameSet
		for _, color_str := range strings.Split(set_str, ",") {
			color_spec := strings.Split(color_str, " ")
			value, err := strconv.Atoi(color_spec[1])
			if err != nil {
				return game, fmt.Errorf(
					"invalid color spec: '%s'", color_spec[1])
			}
			switch color_spec[2] {
			case "red":
				gs.R = value
			case "green":
				gs.G = value
			case "blue":
				gs.B = value
			default:
				return game, fmt.Errorf("invalid color spec: %v", color_spec[2])
			}
		}
		game.Sets = append(game.Sets, gs)
	}

	return game, nil
}

func (game Game) isGamePossible(total GameSet) bool {
	for _, gs := range game.Sets {
		if gs.R > total.R || gs.G > total.G || gs.B > total.B {
			return false
		}
	}
	return true
}

func (game Game) getMinimalCubeSet() GameSet {
	var result = GameSet{0, 0, 0}
	for _, gs := range game.Sets {
		if result.R < gs.R {
			result.R = gs.R
		}
		if result.G < gs.G {
			result.G = gs.G
		}
		if result.B < gs.B {
			result.B = gs.B
		}
	}
	return result
}

func main() {
	var input_file string
	if len(os.Args) > 1 {
		input_file = os.Args[1]
	} else {
		input_file = "input"
	}

	file, err := os.Open(input_file)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// number of (red, greed, blue) cubes in the bag
	var total = GameSet{12, 13, 14}

	scanner := bufio.NewScanner(file)
	resultP1 := 0
	resultP2 := 0
	for scanner.Scan() {
		line := scanner.Text()
		game, _ := parseGameLine(line)
		if game.isGamePossible(total) {
			resultP1 += game.Id
		}
		minGs := game.getMinimalCubeSet()
		power := minGs.R * minGs.G * minGs.B
		resultP2 += power
	}
	fmt.Printf("part 1: %d\npart 2: %d\n", resultP1, resultP2)
}
