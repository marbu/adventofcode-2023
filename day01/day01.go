package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strings"
	"unicode"
)

var number_str = []string{"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"}

func getCalVal(line_raw string) (int, error) {
	var numbers [2]int
	num_count := 0
	for i, ch := range line_raw {
		var digit int
		var found bool
		if unicode.IsDigit(ch) {
			digit = int(ch - '0')
			num_count++
		} else if unicode.IsLetter(ch) {
			found = false
			for j, dw := range number_str {
				if strings.HasPrefix(line_raw[i:], dw) {
					digit = j + 1
					num_count++
					found = true
					break
				}
			}
			if !found {
				continue
			}
		} else {
			continue
		}
		if num_count == 1 {
			numbers[0] = digit
		} else {
			numbers[1] = digit
		}
	}
	if num_count == 0 {
		return 0, errors.New("nothing")
	}
	if num_count == 1 {
		numbers[1] = numbers[0]
	}
	return numbers[0]*10 + numbers[1], nil
}

func main() {
	var input_file string
	if len(os.Args) > 1 {
		input_file = os.Args[1]
	} else {
		input_file = "input"
	}
	file, err := os.Open(input_file)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	result := 0
	for scanner.Scan() {
		cal_val, err := getCalVal(scanner.Text())
		if err == nil {
			result += cal_val
		}
	}
	fmt.Println(result)
}
